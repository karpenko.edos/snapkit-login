import got, { OptionsOfJSONResponseBody } from 'got'
import { HTTPError } from 'got'
import * as pug from 'pug'
import * as fs from 'fs/promises'
import * as pathUtils from 'path'
import * as uuid from 'uuid';
import { Config } from './config'

interface PageData {
    type: string,
    title: string,
    space: {
        key: string
    },
    body: {
        storage: {
            value: string,
            representation: string
        }
    }
}

interface Page extends PageData {
    id: string,
    status: string
}

const searchSprintRetroPage = async (
    config: Config,
    sprintName: string
): Promise<Page | null> => {
    let response
    try {
        response = await got.get(
            `${config.confluence.apiUrl}/content`,
            {
                responseType: 'json',
                searchParams: {
                    title: sprintName,
                    spaceKey: 'parking'
                },
                ...config.confluence.auth,
            }
        ).json<{ results: Page[], size: number }>()

        if (response.size === 0) return null

        // console.log(response.results[0])

        return response.results[0]
    } catch (err) {
        if (err instanceof HTTPError) {
            throw new Error(`Ошибка во время поиска существующей статьи релиза: ${err.message}`)
        } else {
            throw err
        }
    }
}

const makeConfluencePageUrl = (config: Config, id: string) => {
    return `${config.confluence.baseUrl}/pages/viewpage.action?pageId=${id}`
}

const getPage = async (
    config: Config,
    id: string,
    withContent: boolean = false
): Promise<Page> => {
    let page: Page
    try {
        const requestOptions: OptionsOfJSONResponseBody = {
            responseType: 'json',
            ...config.confluence.auth,
        }

        if (withContent) {
            requestOptions.searchParams = {
                expand: 'body.storage'
            };
        }
        page = await got.get(
            `${config.confluence.apiUrl}/content/${id}`,
            requestOptions
        ).json<Page>()

        // console.log(page)
    } catch (err) {
        if (err instanceof HTTPError) {
            throw new Error(`Не удалось получить статью: ${err.message}`)
        } else {
            throw err
        }
    }

    return page
}

const renderPage = async (
    config: Config,
    sprintName: string
): Promise<string> => {
    const rawTemplate = await fs.readFile(
        pathUtils.join(module.path, '../templates/sprint-retro-page.pug'),
        'utf-8'
    )

    const pageTemplate = pug.compile(
        rawTemplate
    )

    const pageContent = pageTemplate({
        uuid: uuid.v4,
        config,
        sprintName
    })

    // console.log(pageContent)
    return pageContent
}

const createPage = async (
    config: Config,
    sprintName: string,
    parentPage: Page,
    content: string
): Promise<Page> => {
    let page: Page
    try {
        page = await got.post(
            `${config.confluence.apiUrl}/content`,
            {
                responseType: 'json',
                json: {
                    type: 'page',
                    title: sprintName,
                    ancestors: [{ id: parentPage.id }],
                    space: { key: parentPage.space.key },
                    body: {
                        storage: {
                            value: content,
                            representation: 'storage'
                        }
                    }
                },
                ...config.confluence.auth,
            }
        ).json<Page>()

        // console.log(page)
    } catch (err) {
        if (err instanceof HTTPError) {
            throw new Error(`Не удалось получить статью: ${err.message}`)
        } else {
            throw err
        }
    }

    return page
}

export default {
    searchSprintRetroPage,
    makeConfluencePageUrl,
    getPage,
    renderPage,
    createPage
}
