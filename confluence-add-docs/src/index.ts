import { Config, readConfig } from './config';
import confluence from './confluence';
import jira, { Issue } from './jira';
import * as pMap from 'p-map';


const filterClosedSprintIssues = async (config: Config): Promise<void> => {
    const issues = await jira.findIssues(config);
    if (issues.length > 0) {
        console.log(`Целевой спринт: "${config.target.jiraSprintName}" (${issues.length} задач)`)
    } else {
        throw new Error(`Целевой спринт "${config.target.jiraSprintName}" не содержит задач. Вероятно, что-то пошло не так`)
    }

    const sprintInfo = await jira.findSprint(config, config.target.jiraSprintName)
    if (sprintInfo.state !== jira.SprintState.Closed) {
        throw new Error(`Целевой спринт не является закрытым. Текущий статус ${sprintInfo.state}`)
    }

    const issuesMapByStatus = new Map<string, Issue[]>();
    issues.forEach((issue) => {
        const status = issue.fields.status.name
        const current = issuesMapByStatus.get(status)
        issuesMapByStatus.set(
            status,
            current ? current.concat(issue) : [issue]
        )
    })
    console.log('\nСтатистика по задачам спринта')
    for (const status of issuesMapByStatus.keys()) {
        console.log(`* ${status}: ${issuesMapByStatus.get(status)?.length}`)
    }

    // ищем задачи на выход из спринта
    const issuesToRemoveSprint: Issue[] = [
        jira.IssueStatusName.ReadyForTest,
        jira.IssueStatusName.Testing
    ]
        .reduce((acc, status) => {
            const statusIssues = issuesMapByStatus.get(status)
            return statusIssues ? acc.concat(statusIssues) : acc
        }, [] as Issue[])
        .filter((issue) => {
            return issue.sprints
                .filter(
                    (sprint) => (sprint.state === jira.SprintState.Active)
                )
                .length > 0;
        })

    if (issuesToRemoveSprint.length) {
        console.log('\nЗадачи в которых надо удалить активные спринты:')
        issuesToRemoveSprint
            .forEach((issue) => {
                console.log(
                    `* [${issue.fields.status.name}] "${issue.fields.summary}" (${
                        jira.makeIssueUrl(config, issue.key)
                    }) - ${
                        issue.sprints
                            .filter(
                                (sprint) => (sprint.state === jira.SprintState.Active)
                            )
                            .map((sprint) => (sprint.name))
                            .join(', ')
                    }`
                )
            })

        await pMap(
            issuesToRemoveSprint,
            async (issue: Issue) => {
                return jira.removeIssueActiveSprint(config, issue)
            },
            { concurrency: 1 }
        )
    } else {
        console.log('\nНет задач в которых надо удалить активные спринты')
    }
}

const createSprintRetroPage = async (config: Config, sprintName: string) => {
    const currentPage = await confluence.searchSprintRetroPage(config, sprintName)
    if (currentPage) {
        console.log(`Все уже сделано: ${confluence.makeConfluencePageUrl(config, currentPage.id)}`)
        return
    }

    const parentPage = await confluence.getPage(
        config,
        config.target.confluenceParentPageId
    )
    if (!parentPage) {
        throw new Error(`Родительская страница для релиза не найдена`)
    }

    const pageContent = await confluence.renderPage(
        config,
        sprintName
    )

    const releasePage = await confluence.createPage(
        config,
        sprintName,
        parentPage,
        pageContent
    )
    console.log(`Создана страница: ${confluence.makeConfluencePageUrl(config, releasePage.id)}`)
}


export const Scenarios = {
    CreateActiveSprintRetroPage: 'CreateActiveSprintRetroPage',
    UpdateClosedSprintRetroPage: 'UpdateClosedSprintRetroPage',
    FilterClosedSprintIssues: 'FilterClosedSprintIssues'
} as const;
export type Scenarios = typeof Scenarios[keyof typeof Scenarios];

const main = async () => {
    const config = await readConfig()

    const project = await jira.getProject(config, config.target.jiraProjectKey)
    console.log(`Целевой проект: "${project.name}" (${project.id})`)

    const scenario = process.env.TARGET_SCENARIO

    if (scenario === Scenarios.CreateActiveSprintRetroPage) {
        const activeSprint = await jira.findActiveSprint(config);
        console.log(`Активный спринт: ${activeSprint.name}`)
        await createSprintRetroPage(config, activeSprint.name);

    } else if (scenario === Scenarios.FilterClosedSprintIssues) {
        await filterClosedSprintIssues(config)

    } else {
        throw new Error(`Сценарий не поддерживается: ${scenario}`)
    }
}

process
    .on('unhandledRejection', (reason, p) => {
        console.error(reason, 'Unhandled Rejection at Promise', p);
    })
    .on('uncaughtException', err => {
        console.error('Uncaught Exception thrown:', err.stack || err);
        process.exit(1);
    });

main().then().catch((err) => {
    console.log(err)
    process.exit(1)
})
