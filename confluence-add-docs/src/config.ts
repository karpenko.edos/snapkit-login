import * as fs from 'fs/promises'

export type AuthData = {
    username: string,
    password: string,
}

export type TargetUser = {
    login: string,
    name: string
}

export type Config = {
    jira: {
        baseUrl: string,
        apiUrl: string,
        auth?: AuthData,
    },
    confluence: {
        baseUrl: string,
        apiUrl: string,
        serverId: string,
        auth?: AuthData,
    },
    target: {
        jiraProjectKey: string,
        jiraSprintName: string,
        confluenceParentPageId: string,
        users: TargetUser[]
    }
}

export const readConfig = async (): Promise<Config> => {
    const configPath = process.env.CONFIG_PATH
    if (typeof configPath === 'undefined') {
        throw new Error('Не указан путь к файлу конфигурации')
    }

    let configContent: string
    let config: Config
    try {
        configContent = await fs.readFile(configPath, 'utf-8')
        config = JSON.parse(configContent) as Config
    } catch (err) {
        throw new Error('Не удалось прочитать конфигурацию')
    }

    if (
        typeof process.env.JIRA_USERNAME === 'string' &&
        typeof process.env.JIRA_PASSWORD === 'string'
    ) {
        config.jira.auth = {
            username: process.env.JIRA_USERNAME,
            password: process.env.JIRA_PASSWORD,
        }
    } else {
        throw new Error('Не заданы учетные данные для JIRA')
    }

    if (
        typeof process.env.CONFLUENCE_USERNAME === 'string' &&
        typeof process.env.CONFLUENCE_PASSWORD === 'string'
    ) {
        config.confluence.auth = {
            username: String(process.env.CONFLUENCE_USERNAME),
            password: String(process.env.CONFLUENCE_PASSWORD),
        }
    } else {
        throw new Error('Не заданы учетные данные для Confluence')
    }

    if (typeof process.env.JIRA_SPRINT_NAME === 'string') {
        config.target.jiraSprintName = process.env.JIRA_SPRINT_NAME
    } else if (!config.target.jiraSprintName) {
        throw new Error('Не задано имя целевого спринта')
    }

    return config
}
