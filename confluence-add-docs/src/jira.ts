import got from 'got'
import { HTTPError } from 'got'
import { Config } from './config'


export type Project = {
    key: string,
    id: string,
    name: string
}


export const SprintState = {
    Active: 'ACTIVE',
    Closed: 'CLOSED'
} as const;
export type SprintState = typeof SprintState[keyof typeof SprintState];

type Sprint = {
    id: number,
    state: string,
    name: string,
    startDate: string,
    endDate: string
}


export const IssueStatusName = {
    ToDo: 'Сделать',
    Open: 'Открытый',
    Reopened: 'Переоткрыт',
    InProgress: 'В работе',
    ReadyForReview: 'Ready for Review',
    Review: 'Review',
    Blocked: 'Blocked',
    ReadyForTest: 'Ready for Test',
    Testing: 'Testing',
    Done: 'Готово',
} as const;
export type IssueStatusName = typeof IssueStatusName[keyof typeof IssueStatusName];

type IssueFields = {
    project: {
        id: string
    },
    status: {
        name: IssueStatusName
    },
    summary: string,
    // sprints custom field
    customfield_10105: string[]
}

export type Issue = {
    key: string,
    sprints: Sprint[],
    fields: IssueFields
}


const getProject = async (config: Config, key: string): Promise<Project> => {
    let project: Project;

    try {
        project = await got.get(
            `${config.jira.apiUrl}/project/${key}`,
            {
                responseType: 'json',
                ...config.jira.auth
            }
        ).json<Project>()

        // console.log(project)
    } catch (err) {
        if (err instanceof HTTPError) {
            throw new Error(`Не удалось получить целевой проект ${key}: ${err.message}`)
        } else {
            throw err
        }
    }

    return project;
}

// example
// com.atlassian.greenhopper.service.sprint.Sprint@1cbb1fb[activatedDate=2024-02-02T15:36:11.159+03:00,autoStartStop=false,completeDate=2024-02-09T14:17:59.964+03:00,endDate=2024-02-09T14:00:00.000+03:00,goal=,id=995,incompleteIssuesDestinationId=<null>,name=UN Sprint 230,rapidViewId=22,sequence=995,startDate=2024-02-02T14:00:00.000+03:00,state=CLOSED,synced=false]
const _parseSprint = (info: string): Sprint => {
    const sprintParts = info
        .split('[')[1].split(']')[0]
        .split(',')
        .map((part) => (part.split('=')[1]))

    if (sprintParts.length === 0) {
        throw new Error('Broken sprint description')
    }

    return {
        id: parseInt(sprintParts[5]),
        state: sprintParts[11],
        name: sprintParts[7],
        startDate: sprintParts[10],
        endDate: sprintParts[3]
    };
}

const _issuesRequest = async (
    config: Config, jql: string, maxResults: number
): Promise<Issue[]> => {
    let response = await got.get(
        `${config.jira.apiUrl}/search`,
        {
            responseType: 'json',
            searchParams: {
                // jql: `project = ${config.target.jiraProjectKey
                //     } and Sprint = "${config.target.jiraSprintName
                //     }"`,
                jql,
                maxResults: 1000
            },
            ...config.jira.auth
        }
    ).json<{ issues: Issue[] }>()
    const issues = response.issues as Issue[]

    issues.map((issue) => {
        issue.sprints = issue.fields.customfield_10105.map(
            (sprint) => (_parseSprint(sprint))
        );
        return issue;
    })

    return issues;
}

const findIssues = async (
    config: Config
): Promise<Issue[]> => {
    const issues = await _issuesRequest(
        config,
        `project = ${
            config.target.jiraProjectKey
        } and Sprint = "${
            config.target.jiraSprintName
        }"`,
        1000
    )

    return issues;
}

const findActiveSprint = async (
    config: Config
): Promise<Sprint> => {
    const issues = await _issuesRequest(
        config,
        `project = ${
            config.target.jiraProjectKey
        } and issuetype in (Task, Bug) and Sprint in openSprints()`,
        1
    )

    const activeSprints = issues[0].sprints
        .filter((sprint) => (sprint.state === SprintState.Active));

    if (activeSprints.length === 0) {
        throw new Error('Не удалось найти активный спринт');
    } else if (activeSprints.length > 1) {
        throw new Error(`Нашлось больше одного активного спринта: ${
            activeSprints.map(({name}) => (name)).join(', ')
        }`);
    }

    return activeSprints[0];
}

const findSprint = async (
    config: Config,
    sprintName: string
): Promise<Sprint> => {
    const issues = await _issuesRequest(
        config,
        `project = ${
            config.target.jiraProjectKey
        } and Sprint = "${sprintName}"`,
        1
    )

    const sprints = issues[0].sprints
        .filter((sprint) => (sprint.name === sprintName));

    if (sprints.length === 0) {
        throw new Error('Не удалось найти активный спринт');
    }

    return sprints[0];
}

const removeIssueActiveSprint = async (
    config: Config,
    issue: Issue
): Promise<void> => {
    try {
        const response = await got.put(
            `${config.jira.apiUrl}/issue/${issue.key}`,
            {
                json: {
                    fields: {
                        customfield_10105: null
                    }
                },
                responseType: 'json',
                ...config.jira.auth
            }
        ).json<{}>()

        console.log(response)
    } catch (err) {
        console.log(err)
    }
}

const makeIssueUrl = (config: Config, key: string) => {
    return `${config.jira.baseUrl}/browse/${key}`
}


export default {
    getProject,
    findIssues,
    makeIssueUrl,
    findActiveSprint,
    findSprint,
    removeIssueActiveSprint,

    IssueStatusName,
    SprintState
}
