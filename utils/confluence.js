const got = require('got');
const {HTTPError, RequestError} = require('got');
const _ = require('underscore');
const debug = require('debug')('confluence');

class Confluence {

	/**
	 * @param {string} baseUrl
	 * @param {object} auth
	 * @param {string} auth.username
	 * @param {string} auth.password
	 */
	constructor({baseUrl, serverId, auth}) {
		this.baseUrl = baseUrl;
		this.apiUrl = `${baseUrl}/rest/api`;
		this.serverId = serverId;
		this.auth = auth;
	}

	async _request(method, url, params={}) {
		debug(`send request: ${method} ${url}, ${JSON.stringify(params)}`);
		try {
			const response = await got[method](
				url,
				{
					responseType: 'json',
					...this.auth,
					...params
				}
			).json();

			return response;
		} catch (err) {
			if (err instanceof HTTPError) {
				throw new Error(`Error during request url ${url} with params ${
					JSON.stringify(params)
				}: ${err.message}`);
			} else {
				throw err;
			}
		}
	}

	_parsePage(page) {
		const result = _(page).pick('id', 'type', 'status', 'title');

		if (page.body?.storage?.value) {
			result.content = page.body.storage.value;
		}

		return result;
	}

	async searchPage(searchParams) {
		const response = await this._request(
			'get',
			`${this.apiUrl}/content`,
			{searchParams}
		);

		return _(response.results).map(this._parsePage);
	}

	async getPage(id, expand=null) {
		const params = {};
		if (expand) {
			params.searchParams = {expand};
		}

		const response = await this._request(
			'get',
			`${this.apiUrl}/content/${id}`,
			params
		);

		return this._parsePage(response);
	}

	async getPageSafe(id, expand=null) {
		try {
			return await this.getPage(id, expand);
		} catch(err) {
			console.log()
			console.log(err);
		}
	}

	async createPage(title, content, spaceKey, parentId=null) {
		const params = {
			json: {
				type: 'page',
				title,
				space: { key: spaceKey },
				body: {
                    storage: {
                        value: content,
                        representation: 'storage'
                    }
                }
			}
		};
		if (parentId) {
			params.json.ancestors = [{id: parentId}];
		}

		const response = await this._request(
			'post',
			`${this.apiUrl}/content`,
			params
		);

		// console.log(this.makePageUrl(response.id))
		return this._parsePage(response);
	}

	makePageUrl(id) {
	    return `${this.baseUrl}/pages/viewpage.action?pageId=${id}`;
	}

}


module.exports = Confluence;

// test
if (require.main === module) {
/*
DEBUG=config,confluence CONFIG_PATH=create-release-ticket/sample-config.json node -r dotenv/config utils/confluence.js
*/

	const ConfigManager = require('./configManager');
	const testEnvConfigFilePath = 'CONFIG_PATH';
	const testEnvMapping = {
		CONFLUENCE_USERNAME: { path: 'confluence.auth.username', required: true },
		CONFLUENCE_PASSWORD: { path: 'confluence.auth.password', required: true }
	};

	const test = async () => {
		const configManager = new ConfigManager(testEnvConfigFilePath, testEnvMapping);
		const {confluence: confluenceConfig} = await configManager.init();

		confluence = new Confluence(confluenceConfig);

		// console.log('makeIssueUrl:', jira.makeIssueUrl('UN-555'))
		console.log('searchPage:', await confluence.searchPage({
            title: 'UN Sprint 245',
            spaceKey: 'parking'
        }))

		// console.log('getPage:', await confluence.getPage('1535286486'))
		console.log('getPage with expand:', await confluence.getPage('1535286486', 'body.storage'))

		// console.log('createPage:', await confluence.createPage('test', 'test-test', 'parking'))
		// console.log('createPage child:', await confluence.createPage('test child', 'test-test', 'parking', '1535294771'))

	}
	test().catch(console.log)
}
