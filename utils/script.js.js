const runScript = async (script) => {
	process
		.on('unhandledRejection', (reason, p) => {
			console.error(reason, 'Unhandled Rejection at Promise', p)
			process.exit(1)
		})
		.on('uncaughtException', err => {
			console.error('Uncaught Exception thrown:', err.stack || err)
			process.exit(1)
		})

	try {
		await script();
	} catch (err) {
		console.log(err.stack || err);
		process.exit(1);
	}
};

exports.runScript = runScript;
