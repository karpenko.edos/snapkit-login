const got = require('got');
const {HTTPError} = require('got');
const _ = require('underscore');
const debug = require('debug')('jira');

const parseWorklogItem = (item) => {
    return {
        started: new Date(item.started),
        startedRaw: item.started,
        timeSpentSeconds: item.timeSpentSeconds,
        author: item.updateAuthor.name,
    };
};

const FIELDS_MAPPING = {
	assignee: 'assignee',
	buildVersion: 'customfield_10505',
	changelogInfo: 'customfield_10400',
	changetype: 'customfield_10401',
	components: 'components',
	created: 'created',
	description: 'description',
	epic: 'customfield_10101',
	fixVersions: 'fixVersions',
	issuetype: 'issuetype',
	labels: 'labels',
	migrations: 'customfield_10503',
	notes: 'customfield_10504',
	parent: 'parent',
	points: 'customfield_10106',
	project: 'project',
	sprints: 'customfield_10105',
	status: 'status',
	summary: 'summary',
	updated: 'updated',
	worklog: 'worklog'
};
const INVERT_FIELDS_MAPPING = _(FIELDS_MAPPING).invert();
const WORK_TYPE_COMPONENTS = [
    'business',
    'engineering',
    'project_support',
    'unplanned'
];
const FIELD_PARSERS = {
	assignee: (value) => {
		return {
			assignee: value.name
		};
	},
	sprints: (value) => {
		const sprints = _(value).map((info) => {
			// com.atlassian.greenhopper.service.sprint.Sprint@1cbb1fb[activatedDate=2024-02-02T15:36:11.159+03:00,autoStartStop=false,completeDate=2024-02-09T14:17:59.964+03:00,endDate=2024-02-09T14:00:00.000+03:00,goal=,id=995,incompleteIssuesDestinationId=<null>,name=UN Sprint 230,rapidViewId=22,sequence=995,startDate=2024-02-02T14:00:00.000+03:00,state=CLOSED,synced=false]
		    const sprintParts = info
		        .split('[')[1].split(']')[0]
		        .split(',')
		        .map((part) => (part.split('=')[1]))

		    if (sprintParts.length === 0) {
		        throw new Error('Broken sprint description')
		    }

		    return {
		        id: parseInt(sprintParts[5], 10),
		        state: sprintParts[11],
		        name: sprintParts[7],
		        startDate: sprintParts[10],
		        endDate: sprintParts[3]
		    };
		})

		return {
			sprints: sprints,
			activeSprint: _(sprints).find(({state}) => (state === 'ACTIVE'))
		};
	},
	status: (value) => {
		return {
			status: _(value).pick('id', 'name')
		};
	},
	project: (value) => {
		return {
			project: _(value).pick('id', 'key')
		};
	},
	issuetype: (value) => {
		return {
			issuetype: _(value).pick('id', 'name', 'subtask')
		};
	},
	components: (value) => {
		const workTypeComponents = (value || []).filter((component) => {
	        return WORK_TYPE_COMPONENTS.some((requiredComponent) => {
	            return requiredComponent === component.name;
	        });
	    });

		let workType = 'unrecognized';
		if (workTypeComponents.length === 1) {
			workType = workTypeComponents[0].name;
		}

		return {
			workType,
			components: (value || []).map(({id, name}) => ({id, name}))
		};
	},
	worklog: (value) => {
		return {
			worklog: value.worklogs.map(parseWorklogItem),
			isFullWorklog: value.maxResults > value.total
		};
	},
	points: (value) => {
		return {
			points: value ? parseInt(value, 10) : 0
		};
	}
};

class Jira {

	/**
	 * @param {string} baseUrl
	 * @param {object} auth
	 * @param {string} auth.username
	 * @param {string} auth.password
	 */
	constructor({baseUrl, auth}) {
		this.baseUrl = baseUrl;
		this.apiUrl = `${baseUrl}/rest/api/2`;
		this.auth = auth;
	}

	async _request(method, url, params={}) {
		debug(`send request: ${method}, ${url}, ${JSON.stringify(params)}`);
		try {
			const response = await got[method](
				url,
				{
					responseType: 'json',
					...this.auth,
					...params
				}
			).json();

			return response;
		} catch (err) {
			if (err instanceof HTTPError) {
				throw new Error(`Error during request url ${url} with params ${JSON.stringify(params)}: ${err.message}`)
			} else {
				throw err
			}
		}
	}

	_parseIssue(issue) {
		const result = {
			key: issue.key,
			id: issue.id
		};

		const fields = _(FIELDS_MAPPING).each((v, k) => {
			if (_(issue.fields).has(v)) {
				if (FIELD_PARSERS[k]) {
					_(result).extend(FIELD_PARSERS[k](issue.fields[v]));
				} else {
					result[k] = issue.fields[v];
				}
			}
		});

		return result;
	}

	_makeRequestFields = (fields) => {
		return _(fields).map((name) => {
			if (!FIELDS_MAPPING[name]) {
				throw new Error(`Unknown field: ${name}`);
			}
			return FIELDS_MAPPING[name];
		}).join(',');
	}

	/**
	 * Returns parsed issues by key
	 * @param {string} key
	 * @param {string[]} [fields]
	 * @param {boolean} [parse=true]
	 */
	async getIssue(key, fields, parse=true) {
		const params = {};
		if (fields) {
			params.searchParams = {
				fields: this._makeRequestFields(fields)
			}
		}

		const response = await this._request(
			'get',
			`${this.apiUrl}/issue/${key}`,
			params
		);

		return parse ? this._parseIssue(response) : response;
	}

	/**
	 * Returns parsed issues list found by given jql
	 * @param {string} jql
	 * @param {string[]} fields - list of fields
	 * @param {number} [maxResults=1000]
	 */
	async getIssues(jql, fields, maxResults=1000) {
		const params = {
			searchParams: {
				jql,
				maxResults
			}
		};
		if (fields) {
			params.searchParams.fields = this._makeRequestFields(fields);
		}

		const response = await this._request(
			'get',
			`${this.apiUrl}/search`,
			params
		);

		// console.log(response.issues)
		return _(response.issues).map(this._parseIssue);
	}

	/**
	 * Returns parsed worklog of issue
	 * @param {string} key
	 */
	async getIssueWorklog(key) {
		const response = await this._request(
			'get',
			`${this.apiUrl}/issue/${key}/worklog`
		);

		const worklogs = response.worklogs.map(parseWorklogItem);

	    return worklogs
	}

	/**
	 * Returns url for issue
	 * @param {string} key
	 */
	makeIssueUrl(key) {
		return `${this.baseUrl}/browse/${key}`;
	}

	/**
	 * Returns parsed project
	 * @param {string} key
	 */
	async getProject(key) {
		const response = await this._request(
			'get',
			`${this.apiUrl}/project/${key}`
		);

	    return _(response).pick('key', 'id', 'name');
	}

	/**
	 * Returns parsed versions
	 * @param {string} projectKey
	 * @param {string} [search]
	 * @param {string} [status] [released, unreleased, archive]
	 */
	async getVersions(projectKey, search=null, status=null) {
		const searchParams = {
			orderBy: 'sequence'
		};
		if (search) {
			searchParams.query = search;
		}
		if (status) {
			searchParams.status = status;
		}

		const response = await this._request(
			'get',
			`${this.apiUrl}/project/${projectKey}/versions`,
			{
				searchParams
			}
		);


		const searchRegexp = new RegExp(search || '', 'i')
	    const versions = _(response).chain()
		    .map((v) => {
		    	let versionStatus;
		    	if (v.archived) versionStatus = 'archive';
		    	if (!versionStatus && v.released) versionStatus = 'released';
		    	if (!versionStatus) versionStatus = 'unreleased';

		    	return {
		    		..._(v).pick('id', 'name', 'description', 'archived', 'released'),
		    		status: versionStatus
		    	};
		    })
		    // BUT!!! jira filters don't work, let's filter on our side
		    .filter((v) => {
		    	if (status && v.status !== status) {
		    		return false;
		    	}

		    	if (search && !searchRegexp.test(v.name)) {
		    		return false;
		    	}

		    	return true;
		    })
		    .value();

	    return versions;
	}

	makeVersionUrl(projectKey, releaseId) {
	    return `${this.baseUrl}/projects/${projectKey}/versions/${releaseId}`;
	}
}

module.exports = Jira;
exports.WORK_TYPE_COMPONENTS = WORK_TYPE_COMPONENTS;

// test
if (require.main === module) {
/*
DEBUG=config,jira CONFIG_PATH=project-analytics/sample-config.json node -r dotenv/config utils/jira.js
*/

	const ConfigManager = require('./configManager');
	const testEnvConfigFilePath = 'CONFIG_PATH';
	const testEnvMapping = {
		JIRA_USERNAME: { path: 'jira.auth.username', required: true },
		JIRA_PASSWORD: { path: 'jira.auth.password', required: true }
	};

	const test = async () => {
		const configManager = new ConfigManager(testEnvConfigFilePath, testEnvMapping);
		const {jira: jiraConfig} = await configManager.init();

		jira = new Jira(jiraConfig);

		// console.log('makeIssueUrl:', jira.makeIssueUrl('UN-555'))

		// console.log('getIssue:', await jira.getIssue('UN-555', null, false))
		// console.log('getIssue with fields:', await jira.getIssue(
		// 	'UN-555',
		// 	['status', 'summary', 'changelogInfo', 'worklog']
		// ))

		// console.log('getIssues with fields:', await jira.getIssues(
		// 	'project = UN and sprint = "UN Sprint 247"',
		// 	['status', 'summary', 'changelogInfo'],
		// 	3
		// ))

		// console.log('getIssueWorklog:', await jira.getIssueWorklog('UN-777'))

		console.log('getProject:', await jira.getProject('UN'))

		// console.log('getVersions:', await jira.getVersions('UN'))
		console.log('getVersions with search:', await jira.getVersions('UN', 'РФ УДС 130'))
		console.log('getVersions with status:', await jira.getVersions('UN', null, 'unreleased'))

		console.log('makeVersionUrl:', jira.makeVersionUrl('UN', 13944))
	}
	test().catch(console.log)
}
