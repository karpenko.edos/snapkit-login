const fs = require('fs/promises');
const deepExtend = require('deep-extend');
const debug = require('debug')('config');

const readEnvVar = function(name, required) {
	const env = process.env[name];
	if (required && typeof env === 'undefined') {
		throw new Error(`Env variable ${name} is required`);
	}
	return env;
};

const readEnvMapping = function(envMapping) {
	const result = {};

	Object.keys(envMapping).forEach((k) => {
		const env = readEnvVar(k, envMapping[k].required);

		if (env) {
			path = envMapping[k].path.split('.');
			path.reduce((current, key, level, path) => {
				if (level === path.length - 1) {
					current[key] = env;
					return;
				}

				if (!current[key]) {
					current[key] = {};
				}

				return current[key];
			}, result);
		}
	});

	return result;
}

class ConfigManager {

	/**
	 * @param {string} [envConfigFilePath] - name of ENV of config file path
	 * @param {object} [envMapping] - mapping of ENV variables
	 * @param {object} [schema] - JSON schema of config
	 */
	constructor(envConfigFilePath, envMapping, schema) {
		this.envConfigFilePath = envConfigFilePath;
		this.envMapping = envMapping;
		this.schema = schema;

		this.config = {};
	}

	async init() {
		if (this.envConfigFilePath) {
			const configFilePath = readEnvVar(this.envConfigFilePath, true);
			try {
				const configContent = await fs.readFile(configFilePath, 'utf-8');
				this.config = JSON.parse(configContent);
			} catch (err) {
				throw new Error(`Cannot read config file: ${err.message}`)
			}
		}

		if (this.envMapping) {
			const envConfig = readEnvMapping(this.envMapping);
			deepExtend(this.config, envConfig)
		}

		if (this.schema) {
			// @todo
		}

		debug(this.config);
		return this.get();
	}

	get() {
		return this.config;
	}

}

module.exports = ConfigManager;


// test
if (require.main === module) {
	// JIRA_USERNAME=test JIRA_PASSWORD=pass CONFIG_PATH=project-analytics/sample-config.json node utils/configManager.js
	// CONFIG_PATH=project-analytics/sample-config.json node -r dotenv/config utils/configManager.js

	const testEnvConfigFilePath = 'CONFIG_PATH';

	const testEnvMapping = {
		JIRA_USERNAME: { path: 'jira.auth.username', required: true },
		JIRA_PASSWORD: { path: 'jira.auth.password', required: true }
	};

	const testSchema = {
		type: 'object',
		properties: {
			jira: {
				type: 'object',
				properties: {
					auth: {
						type: 'object',
						properties: {
							username: { type: 'string' },
							password: { type: 'string' }
						},
						required: ['var1', 'var2']
					}
				},
				required: ['auth']
			}
		},
		required: ['jira']
	};

	const test = async () => {
		configManager = new ConfigManager(testEnvConfigFilePath, testEnvMapping, testSchema);
		await configManager.init();
		console.log(configManager.get());
	}
	test().catch(console.log)
}