//
//  LoginView.swift
//  SnapLogin
//
//  Created by Eeedik on 18.10.2022.
//

import Foundation
import UIKit
import SnapKit

class LoginView: UIView {
    let containerViewHeight: CGFloat = 192.0
    let topInnerViewHeight: CGFloat = 24.0
    let bottomInnerViewHeight: CGFloat = 36.0
    let tfHeight: CGFloat = 44.0
    let connectButtonWidth: CGFloat = 120.0
    var isAnimating = false
    
    var viewContainer: UIView!
    var viewTop: UIView!
    var viewBottom: UIView!
    var tfEmail: UITextField!
    var tfPassword: UITextField!
    var btnConnect: UIButton!
    var activityIndicator: UIActivityIndicatorView!
    var centerYConstraint: Constraint!
    var timer: Timer!
    
    init() {
        super.init(frame: .zero)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleKeyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleKeyboardDidHide(notification:)),
                                               name: UIResponder.keyboardDidHideNotification,
                                               object: nil)
        setupContainerView()
        setupTitle()
        setupTextfields()
        setupBottomView()
        setupActivityIndicator()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupContainerView() {
        viewContainer = UIView()
        viewContainer.backgroundColor = UIColor(red: 167.0/255.0, green: 177.0/255.0, blue: 180.0/255.0, alpha: 1.0)
        viewContainer.layer.cornerRadius = 10.0
        viewContainer.clipsToBounds = true
        self.addSubview(viewContainer)
        
        viewContainer.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(40).priority(750)
            make.right.equalTo(self).offset(-40).priority(750)
            make.width.lessThanOrEqualTo(500)
            make.centerX.equalTo(self)
            self.centerYConstraint = make.centerY.equalTo(self).constraint
            make.height.equalTo(containerViewHeight)
        }
    }
    
    func setupTitle() {
        viewTop = UIView()
        viewContainer.addSubview(viewTop)
        viewTop.backgroundColor = UIColor(red: 190.0/255.0, green: 195.0/255, blue: 198.0/255.0, alpha: 1.0)
        
        viewTop.snp.makeConstraints { (make) in
            make.left.equalTo(viewContainer)
            make.top.equalTo(viewContainer)
            make.right.equalTo(viewContainer)
            make.height.equalTo(topInnerViewHeight)
        }
        
        let lbTitle = UILabel()
        viewTop.addSubview(lbTitle)
        lbTitle.text = "LOGIN"
        lbTitle.textColor = .white
        lbTitle.backgroundColor = .clear
        lbTitle.font = UIFont(name: "Futura-Bold", size: 17.0)
        
        lbTitle.snp.makeConstraints { make in
            make.edges.equalTo(viewTop).inset(UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0))
        }
    }
    
    func setupTextfields() {
        tfEmail = UITextField()
        viewContainer.addSubview(tfEmail)
        
        tfEmail.delegate = self
//        tfEmail.placeholder = "EMAIL"
//        tfEmail.borderStyle = .none
        tfEmail.backgroundColor = .white
        tfEmail.attributedPlaceholder = NSAttributedString(
            string: "EMAIL",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray]
        )
        tfEmail.keyboardType = .emailAddress
        tfEmail.returnKeyType = .next
        tfEmail.autocapitalizationType = .none
        
        tfEmail.snp.makeConstraints { make in
            make.left.equalTo(viewContainer).offset(8)
            make.top.equalTo(viewTop.snp.bottom).offset(16)
            make.right.equalTo(viewContainer).offset(-8)
            make.height.equalTo(tfHeight)
        }
        
        tfPassword = UITextField()
        viewContainer.addSubview(tfPassword)
        
        tfPassword.delegate = self
//        tfPassword.placeholder = "PASSWORD"
        tfPassword.attributedPlaceholder = NSAttributedString(
            string: "PASSWORD",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray]
        )
//        tfPassword.borderStyle = .none
        tfPassword.backgroundColor = .white
        tfPassword.returnKeyType = .done
        tfPassword.autocapitalizationType = .none
        tfPassword.isSecureTextEntry = true
        
        tfPassword.snp.makeConstraints { make in
            make.left.equalTo(viewContainer).offset(8)
            make.top.equalTo(tfEmail.snp.bottom).offset(8)
            make.right.equalTo(viewContainer).offset(-8)
            make.height.equalTo(tfHeight)
        }
    }
    
    func setupBottomView() {
        viewBottom = UIView()
        viewContainer.addSubview(viewBottom)
        viewBottom.backgroundColor = UIColor(red: 190.0/255.0, green: 195.0/255, blue: 198.0/255.0, alpha: 1.0)
        
        viewBottom.snp.makeConstraints { make in
            make.left.equalTo(viewContainer)
            make.right.equalTo(viewContainer)
            make.bottom.equalTo(viewContainer)
            make.height.equalTo(bottomInnerViewHeight)
        }
        
        btnConnect = UIButton()
        viewBottom.addSubview(btnConnect)
        btnConnect.setTitle("Connect", for: .normal)
        btnConnect.setTitleColor(.white, for: .normal)
        btnConnect.setTitleColor(.lightGray, for: .highlighted)
        btnConnect.backgroundColor = .orange
        btnConnect.titleLabel?.font = UIFont(name: "Futura", size: 15.0)
        btnConnect.addTarget(self, action: #selector(self.connect), for: .touchUpInside)
        
        btnConnect.snp.makeConstraints { make in
            make.top.equalTo(viewBottom)
            make.right.equalTo(viewBottom)
            make.bottom.equalTo(viewBottom)
            make.width.equalTo(connectButtonWidth)
        }
        
    }
    
    func setupActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        viewContainer.addSubview(activityIndicator)
        activityIndicator.color = .orange
        activityIndicator.startAnimating()
        
        activityIndicator.snp.makeConstraints { make in
            make.centerX.equalTo(viewContainer)
            make.centerY.equalTo(viewContainer).offset(-containerViewHeight/2 - 20)
            make.width.equalTo(40)
            make.height.equalTo(self.activityIndicator.snp.width)
        }
    }
    
    @objc func connect() {
        if isAnimating {
            return
        }
        
        tfEmail.snp.remakeConstraints { (make) in
            make.top.equalTo(viewTop.snp.bottom).offset(16)
            make.left.equalTo(viewContainer.snp.right)
            make.width.equalTo(tfEmail.snp.width)
            make.height.equalTo(tfHeight)
        }
        
        tfPassword.snp.remakeConstraints { make in
            make.top.equalTo(tfEmail.snp.bottom).offset(8)
            make.right.equalTo(viewContainer.snp.left)
            make.width.equalTo(tfPassword.snp.width)
            make.height.equalTo(tfHeight)
        }
        
        activityIndicator.snp.updateConstraints { make in
            make.centerY.equalTo(viewContainer)
        }
        
        self.setNeedsLayout()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }) { (finished) in
            self.timer = Timer.scheduledTimer(timeInterval: 3.0,
                                              target: self,
                                              selector: #selector(self.revertLoginView),
                                              userInfo: nil,
                                              repeats: false)
        }
        
        isAnimating = true
    }
    
    @objc func revertLoginView() {
        tfEmail.snp.remakeConstraints { make in
            make.left.equalTo(viewContainer).offset(8.0)
            make.top.equalTo(viewTop.snp.bottom).offset(16.0)
            make.right.equalTo(viewContainer).offset(-8.0)
            make.height.equalTo(tfHeight)
        }
        
        tfPassword.snp.remakeConstraints { make in
            make.top.equalTo(tfEmail.snp.bottom).offset(8.0)
            make.left.equalTo(tfEmail)
            make.right.equalTo(tfEmail)
            make.height.equalTo(tfHeight)
        }
        
        activityIndicator.snp.updateConstraints { make in
            make.centerY.equalTo(viewContainer).offset(-containerViewHeight/2 - 20)
        }
        
        self.setNeedsLayout()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.layoutIfNeeded()
        }) { (finished) in
            if finished {
                self.timer.invalidate()
                self.timer = nil
                
                self.isAnimating = false
            }
        }
    }
    
    @objc func handleKeyboardWillShow(notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String, Any> {
            if let keyboardFrameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardFrame = keyboardFrameValue.cgRectValue
                
                let containerViewOriginPlusHeight = viewContainer.frame.origin.y + viewContainer.frame.size.height
                
                if containerViewOriginPlusHeight > keyboardFrameValue.cgRectValue.origin.y {
                    let overlappedSpace = containerViewOriginPlusHeight - keyboardFrame.origin.y
                    
                    centerYConstraint.update(offset: -overlappedSpace)
                    self.setNeedsLayout()
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self.layoutIfNeeded()
                    })
                }
            }
        }
    }
    
    @objc func handleKeyboardDidHide(notification: Notification) {
        centerYConstraint.update(offset: 0)
        self.setNeedsLayout()
        
        UIView.animate(withDuration: 0.4, animations: {
            self.layoutIfNeeded()
        })
    }
}

extension LoginView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == tfEmail {
            tfPassword.becomeFirstResponder()
        }
        else {
            tfPassword.resignFirstResponder()
        }
        
        return true
    }
}
